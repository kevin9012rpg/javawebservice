package lendle.courses.network.loginws;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Web application lifecycle listener.
 *
 * @author Kevin9012
 */
public class CounterListener implements HttpSessionListener, ServletRequestListener {
    public static int counter=0;

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        counter++;
        // System.out.println("session created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        counter--;
    }

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        ((HttpServletRequest)sre.getServletRequest()).getSession();
    }
}
