<%-- 
    Document   : processLogin
    Created on : 2021/12/29, 下午 04:34:52
    Author     : Kevin9012
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            String id=request.getParameter("id");
            String password=request.getParameter("password");
            
            if("user".equals(id) && "pass".equals(password)){
                session.setAttribute("loggedInUser", id);
                response.sendRedirect("admin/index.jsp");
            }else{
                response.sendRedirect("error.jsp");
            }
        %>        
    </body>
</html>
